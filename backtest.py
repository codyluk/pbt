import pandas as pd
import numpy as np
import datetime
import random
import talib

def Unix2Date(Unix):
    result = []
    for time in Unix:
        result.append(datetime.datetime.fromtimestamp(int(str(time)[:-3])).strftime('%Y-%m-%d %H:%M:%S'))
    return np.asarray(result)

data = pd.read_csv("EOSETH.csv", delimiter=',')

stratTime = data['Start time']
close = data['close']
nclose = np.asarray(close)

EMA = np.asarray(talib.EMA(nclose,5))

stratTime = Unix2Date(stratTime)

result = {"Time":stratTime,"EMA":EMA}

result = pd.DataFrame(result)

result.to_csv("EOSETH_EMA.csv")